open Solvuu_build.Std

let project_name = "myproject"
let version = "dev"

let lib = Project.lib project_name
  ~dir:"lib"
  ~style:(`Pack project_name)
  ~install:(`Findlib project_name)
  ~thread:()
  ~findlib_deps:["core"]

let myapp = Project.app project_name
  ~file:"app/main.ml"
  ~thread:()
  ~internal_deps:[lib]
  ~findlib_deps:["core"]

let () = Project.basic1 ~project_name ~version [lib;myapp]
