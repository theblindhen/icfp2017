OCaml test project
==================

First install the "opam" package manager. It's already installed on the contest
VM, and you can also skip the following steps there:

    $ opam init # Answer `y` at the prompt
    $ ocaml -version
    $ opam switch create 4.04.2 # if version wasn't 4.04.x
    follow instructions after previous command if run

Install some packages we might need:

    $ opam install solvuu-build core batteries async merlin

Now build and run this program:

    $ make native
    $ ./main.native


On subsequent terminal sessions, do the following unless you allowed `opam init`
to modify your shell rc:

    $ eval `opam config env`
