open Core

open Myproject

(* TODO: exercise ppx *)
(* TODO: yojson or atd *)

let greetings =
  Set.singleton String.comparator "Hello World"
  |> Fn.flip Set.add "Hello ICFP"

let () =
  greetings
  |> Set.to_list
  |> Hash_set.of_list ~hashable:Base__Hashtbl.Hashable.poly
  |> Hash_set.to_list
  |> Sequence.of_list
  |> Sequence.iter ~f:(fun greeting ->
      print_endline greeting
    )

let () =
  Stringfun.burrows_wheeler "banana" |>  print_endline
