open Core

let hey x =
      x + 1

let burrows_wheeler s =
  let eof = Char.of_int_exn 255  in
  let sof = Char.of_int_exn 254  in
  let ls = sof :: (String.to_list s ) @ [ eof ] in
  let lss =
    let rec cyclic_shifts passed agg = function
      | [] -> agg
      | e::rem ->
         let passed = passed@[e] in
         cyclic_shifts passed ((rem @ passed)::agg) rem in
    cyclic_shifts [] [] ls
    |> List.sort ~cmp:(List.compare Char.compare) in
  let bw = lss |> List.map ~f:(fun ls -> List.last_exn ls) in
  bw
  |> List.map ~f:(fun e -> if e = eof then '|' else if e = sof then '^' else e)
  |> String.of_char_list
