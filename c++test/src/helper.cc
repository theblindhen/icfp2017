#include <string>
#include <iostream>

#include <Poco/JSON/Object.h>
#include <Poco/JSON/Parser.h>
#include <Poco/Dynamic/Var.h>

#include "helper.h"

using Poco::Dynamic::Var;
using Poco::JSON::Object;

using std::endl;

void helper() {
  std::string json = "{ \"test\" : { \"property\" : \"value\" } }";
  Poco::JSON::Parser parser;
  try {
    Var result = parser.parse(json);

    // use pointers to avoid copying
    Object::Ptr object = result.extract<Object::Ptr>();
    Var test = object->get("test"); // holds { "property" : "value" }
    Object::Ptr subObject = test.extract<Object::Ptr>();
    test = subObject->get("property");
    std::string val = test.toString(); // val holds "value"

    std::cout << val << endl;
  } catch (Poco::RuntimeException &e) {
    std::cout << "JSON error: " << e.message() << endl;
  }
}

