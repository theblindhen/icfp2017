open Core

module GM = GameMessages
module GG = GameGraph
module J = Yojson.Basic.Util

let max_mine_neighbors = 64

let init_istate punter punters map extensions =
  let ggmap = GG.load_map map in
  let dist_map = GG.distance_to_mine ggmap in
  let mine_neighbors = MineNeighbors.mine_neighbors ~map:ggmap ~dist_map:dist_map ~max_edges:max_mine_neighbors in
  { Player. punter ; punters ; map=ggmap ; dist_map ; mine_neighbors ; extensions }

let init_hstate initial_state =
  let rem_options =
    if List.mem initial_state.Player.extensions GM.Options ~equal:(=)  then
      List.length initial_state.Player.map.GG.mines
    else
      0
  in
  { Player. initial_state; moves_rev = [] ; rem_options }

module Make(P : Player.AI) = struct
  
  module State = StateSerializer.Make(struct
    type t = P.state * Player.hstate
  end) 
  
  let input_message channel =
    let len =
      let buf = Buffer.create 9 in
      let rec next () =
        match In_channel.input_char channel with
        | None -> failwith "Unexpected end of input"
        | Some ':' -> ()
        | Some c ->
            Buffer.add_char buf c;
            next ()
      in
      next ();
      Int.of_string (Buffer.contents buf)
    in
    let buf = String.create len in
    Option.value_exn ~message:"Too few bytes in input"
      (In_channel.really_input ~pos:0 ~buf ~len channel);
    Yojson.Basic.from_string buf
  
  let output_message channel json =
    let buf = Yojson.Basic.to_string json in
    let len = String.length buf in
    Out_channel.output_string channel (string_of_int len);
    Out_channel.output_char channel ':';
    Out_channel.output_string channel buf;
    Out_channel.flush channel
  
  let json_of_move move =
    match move with
    | GM.Pass { punterid } ->
        `Assoc [ "pass", `Assoc [ "punter", `Int punterid ] ]
    | GM.Claim { punterid; source; target } ->
        `Assoc [ "claim", `Assoc [
          "punter", `Int punterid;
          "source", `Int source;
          "target", `Int target;
        ] ]
    | GM.Splurge { punterid; start; path } ->
        `Assoc [ "splurge", `Assoc [
          "punter", `Int punterid;
          "route",  `List (List.map (start::path) ~f:(fun s -> `Int s))
        ] ]
    | GM.Option { punterid; source; target } ->
        `Assoc [ "option", `Assoc [
          "punter", `Int punterid;
          "source", `Int source;
          "target", `Int target;
        ] ]
  
  let parse_settings json =
    if json = `Null then
      []
    else
      let is_set name elem =
        match J.member name json with
        | `Null -> None
        | value -> if J.to_bool value then Some elem else None
      in
      [ is_set "splurges" GM.Splurges ;
        is_set "futures"  GM.Futures  ;
        is_set "options"  GM.Options  ;
      ] |> List.filter_opt 
  
  let parse_initial_state json =
    let punter = J.member "punter" json |> J.to_int in
    let punters = J.member "punters" json |> J.to_int in
    let map = J.member "map" json |> GM.parse_map in
    let extensions = J.member "settings" json |> parse_settings
    in (punter, punters, map, extensions)
  
  module MaxScoreDecider = struct
  
    let decide hstate =
      match
        Player.compute_scores hstate
        |> List.filter ~f:(fun (id, _) -> id <> (Player.punter hstate))
        |> List.max_elt ~cmp:(fun (_, s1) (_, s2) -> s1 - s2)
      with
      | None -> failwith "No opponents?"
      | Some (id, _) -> id
  
  end
  
  let setup_request json =
    let (punter, punters, map, extensions) = parse_initial_state json in 
    let initial_state = init_istate punter punters map extensions in
    let map = initial_state.Player.map in
    let hstate = init_hstate initial_state in
    let state = P.setup initial_state in
    let reply =
      `Assoc [
        "ready", `Int punter;
        "state", `String (State.encode (state, hstate));
      ]
    in
    let () =
      let sexts = "[ "^ (List.map extensions ~f:GM.extension_to_string |> String.concat ~sep:", ") ^" ]" in
      Log.log "Game setup:\n\tWe are Punter %d\n\tThere are %d punters\n\tThe map has %d sites and %d mines and %d rivers\n\tExtensions enabled are %s\n" punter punters (GG.G.nb_vertex map.GG.graph) (List.length map.GG.mines) (GG.G.nb_edges map.GG.graph) sexts
    in
    output_message Out_channel.stdout reply
  
  let stop_request json =
    J.member "stop" json 
    |> Yojson.Basic.pretty_to_string
    |> Printf.eprintf "%s\n";
  
    json
    |> J.member "stop"
    |> J.member "scores"
    |> J.to_list
    |> List.map ~f:(fun score -> 
         (J.member "punter" score |> J.to_int,
          J.member "score" score |> J.to_int))
    |> P.stop
  
  let move_request json =
    let moves =
      json
      |> J.member "move"
      |> J.member "moves"
      |> J.to_list
      |> List.to_array
      |> Array.map ~f:(fun move_json ->
            (* Is it a claim? *)
            match J.member "claim" move_json with
            | (`Assoc _) as claim_json ->
                GM.Claim {
                  punterid = J.member "punter" claim_json |> J.to_int;
                  source = J.member "source" claim_json |> J.to_int;
                  target = J.member "target" claim_json |> J.to_int;
                }
            | _ ->
                (* Is it a splurge ? *)
               begin
                 match J.member "splurge" move_json with
                 | (`Assoc _) as splurge_json ->
                    let route = J.member "route" splurge_json
                                |> J.to_list
                                |> List.map ~f:J.to_int
                    in
                    begin
                    match route with
                    | start::tl ->
                       GM.Splurge {
                           punterid = J.member "punter" splurge_json |> J.to_int;
                           start = start;
                           path = tl;
                         }
                    | _ -> failwith "Protocol error" (* TODO: More robust ? *)
                    end
                 | _ ->
                    (* Is it an option ? *)
                    begin
                        match J.member "option" move_json with
                        | (`Assoc _) as option_json ->
                           GM.Option {
                               punterid = J.member "punter" option_json |> J.to_int;
                               source = J.member "source" option_json |> J.to_int;
                               target = J.member "target" option_json |> J.to_int;
                             }
                        | _ ->
                            (* Let's hope it's a Pass *)
                            GM.Pass {
                                punterid =
                                J.member "pass" move_json
                                |> J.member "punter"
                                |> J.to_int;
                            }
                    end
               end
          )
    in
    let state_json = J.member "state" json in
    let state_str = Yojson.Basic.Util.to_string state_json in
    let (state, hstate) = State.decode state_str in
    let punterid = Player.punter hstate in
    Array.iter moves ~f:(fun move ->
      Player.update_hstate hstate move;
      if GM.move_by move = punterid then
        P.we_moved move state
      else
        P.opponent_moved move state
      );
    let move =
      try P.choose_move state hstate with
      | e ->
          Log.log "Caught exception in choose_move. Passing.";
          Log.log "  %s" (Caml.Printexc.to_string e);
          GM.Pass { punterid }
    in
    let move_desc = GM.string_of_move move
    in
    Log.log "Player %d %s\n" (Player.punter hstate) move_desc;
    let encoded_state = State.encode (state, hstate) in
    Player.compute_scores hstate |> Player.print_scores;
    let `Assoc move_assoc = json_of_move move in
    let move_json = `Assoc (("state", `String encoded_state) :: move_assoc) in
    output_message Out_channel.stdout move_json
  
  let remove_nonblock descr =
    let flags = Unix.fcntl_getfl descr in
    let flags = Unix.Open_flags.(flags - nonblock) in
    Unix.fcntl_setfl descr flags
  
  let entry_point () =
    (* Horrible workaround for lamduct bug *)
    remove_nonblock (Unix.descr_of_in_channel In_channel.stdin);
    remove_nonblock (Unix.descr_of_out_channel Out_channel.stdout);
    (* Handshake *)
    let name = match Sys.getenv "NAME" with None -> "TheBlindHen" | Some name -> name in
    output_message Out_channel.stdout (`Assoc [ "me", `String name ]);
    ignore (input_message In_channel.stdin);
    (* Timer starts *)
    let stime = Unix.gettimeofday () in
    let json = input_message In_channel.stdin in
    begin
    if J.member "punter" json <> `Null then
      setup_request json
    else if J.member "move" json <> `Null then
      move_request json
    else if J.member "stop" json <> `Null then
      stop_request json
    else
      failwith ("Request not understood: " ^ Yojson.Basic.to_string json)
    end;
    let etime = Unix.gettimeofday () in
    let time = etime -. stime in
    Log.log "Responded in %fs\n" time
   
end
