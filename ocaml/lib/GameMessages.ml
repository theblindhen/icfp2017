open Core

type name = string
type punterid = int
type siteid = int

type move =
  | Claim of { punterid: punterid; source: siteid; target: siteid }
  | Pass of { punterid: punterid }
  | Splurge of { punterid: punterid ; start: siteid; path: siteid list } (* Note: the "route" is (start::path) *)
  | Option of { punterid: punterid; source: siteid; target: siteid }

let iter_move ~f = function
| Claim { punterid; source; target } -> f ~punterid:punterid ~source:source ~target:target
| Splurge { punterid; start; path } ->
   List.fold path ~init:start
     ~f:(fun prev cur ->
       f ~punterid:punterid ~source:prev ~target:cur ;
       cur
     )
   |> ignore
| Option { punterid; source; target } -> f ~punterid:punterid ~source:source ~target:target 
| Pass _ -> ()

let move_by = function
| Claim { punterid } -> punterid
| Pass { punterid } -> punterid
| Splurge { punterid } -> punterid
| Option { punterid } -> punterid

let set_move_id move id = match move with
| Pass { punterid } -> Pass { punterid = id }
| Claim def -> Claim { def with punterid = id }
| Splurge def -> Splurge { def with punterid = id }
| Option def -> Option { def with punterid = id }

let string_of_route (route : siteid list) : string =
  "[ "^ (route |> List.map ~f:Int.to_string |> String.concat ~sep:"; ") ^" ]"

let string_of_move = function
| Claim { punterid; source; target } -> 
    Printf.sprintf "Claim (id: %d, src: %d, dst: %d)" punterid source target
| Pass { punterid } -> 
    Printf.sprintf "Pass (id: %d)" punterid
| Splurge { punterid; start; path } -> 
    Printf.sprintf "Splurge (id: %d): %s" punterid (string_of_route (start::path))
| Option { punterid; source; target } -> 
    Printf.sprintf "Option (id: %d, src: %d, dst: %d)" punterid source target

type moves = move list

type score = punterid * int
type scores = score list

type noplayers = int
  
type sites = siteid list
type rivers = (siteid * siteid) list
type mines = siteid list

type map = {
  sites: sites;
  rivers: rivers;
  mines: mines
}

type state = punterid * noplayers * map

type extension =
  | Futures
  | Splurges
  | Options

let extension_to_string = function
  | Futures -> "futures"
  | Splurges-> "splurges"
  | Options -> "options"

let parse_map json = 
  let module J = Yojson.Basic.Util in
  let (%>) f g x = g (f x) in
  {
    sites = J.member "sites" json |> J.to_list |> List.map ~f:(
      J.member "id" %> J.to_int
    );
    mines = J.member "mines" json |> J.to_list |> List.map ~f:J.to_int;
    rivers =
      J.member "rivers" json |> J.to_list
      |> List.map ~f:(fun river ->
        (J.member "source" river |> J.to_int),
        (J.member "target" river |> J.to_int)
      )
  }
