open Core
open Graph

module GG = GameGraph
module G = GG.G

(* Load a map from a json file *)
let load_map_from_file (f : string) =
  Yojson.Basic.from_file f
  |> GameMessages.parse_map
  |> GG.load_map

(* Returns a hash table that maps each reachable vertex to its nearest "source"
 * from `sources`. The returned map also maps each source to itself. The
 * `ignored` vertices are treated like they have been removed from the graph.
 * There should be no overlap between `sources` and `ignored`. *)
let multi_source_bfs
    ~(sources: G.V.t list)
    ?(ignored_edges=([]: G.E.t list))
    (g: G.t)
    : (G.V.t, G.V.t) Hashtbl.Poly.t =
  let ignored_edge_map = Hash_set.Poly.create () in
  List.iter ignored_edges ~f:(fun (v1, v2) ->
    Hash_set.Poly.add ignored_edge_map (v1, v2);
    Hash_set.Poly.add ignored_edge_map (v2, v1); (* reversed *)
  );
  let visited = Hash_set.Poly.create ~size:(G.nb_edges g) () in
  List.iter ~f:(Hash_set.Poly.add visited) sources; (* visited += sources *)
  let v_to_source = Hashtbl.Poly.create ~size:(G.nb_edges g) () in
  List.iter sources ~f:(fun source ->
    Hashtbl.Poly.set v_to_source ~key:source ~data:source
  ); (* v_to_source += sources^2 *)
  let queue = Queue.of_list (Hashtbl.Poly.keys v_to_source) in
  while not (Queue.is_empty queue) do
    let v = Queue.dequeue_exn queue in
    let source = Hashtbl.Poly.find_exn v_to_source v in
    List.iter (G.succ g v) ~f:(fun neighbor ->
      if not (Hash_set.Poly.mem visited neighbor) &&
         not (Hash_set.Poly.mem ignored_edge_map (v, neighbor))
      then (
        Hash_set.Poly.add visited neighbor;
        Hashtbl.Poly.add_exn v_to_source ~key:neighbor ~data:source;
        Queue.enqueue queue neighbor;
      )
    );
  done;
  v_to_source


(* Displaying a map *)
let dot_mine_color = 0x00AA00
let dot_output_map (oc : Caml.out_channel) (map : GG.map) =
  let mines = Hash_set.Poly.of_list map.GG.mines in
  let module DisplayMap = struct
    include GG.G
    let vertex_name v = string_of_int (V.label v)
    let graph_attributes _ = []
    let default_vertex_attributes _ = []
    let vertex_attributes v =
      if Hash_set.Poly.mem mines v then
        [ `Color dot_mine_color ; `Shape `Diamond ]
      else
        []
    let default_edge_attributes _ = []
    let edge_attributes e = []
    let get_subgraph _ = None
  end in
  let module Neato = Graphviz.Neato(DisplayMap) in
  Neato.output_graph oc map.GG.graph

let display_with map =
  MyUtil.display_dot_with (fun oc -> dot_output_map oc map)
