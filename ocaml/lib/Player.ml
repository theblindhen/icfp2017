open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G
module CG = ComponentGraph
module H = Hashtbl.Poly
module HSet = Hash_set.Poly

type istate = 
  { 
    punter : GM.punterid; 
    punters : int; 
    map : GG.map ;
    dist_map : GG.mine_vertex_map ; (* Distance btw. any (mine * vertex) *)
    extensions : GM.extension list ;
    mine_neighbors : MineNeighbors.mine_neighbors ;
  }

type hstate =
  { 
    initial_state : istate; 
    mutable moves_rev : GM.move list ;
    mutable rem_options : int ;
  }

let update_hstate hstate move =
  hstate.moves_rev <- move::hstate.moves_rev;
  if GM.move_by move = hstate.initial_state.punter then
    match move with
    | GM.Option _ -> hstate.rem_options <- hstate.rem_options - 1
    | _ -> ()

let punter ({ initial_state } : hstate) =
  initial_state.punter

let set_punter hstate id =
  let { initial_state } = hstate in
  { hstate with initial_state = { initial_state with punter = id } }

let compute_scores hstate : (GM.punterid * int) list =
  let { initial_state; moves_rev } = hstate in
  let { punters; map } = initial_state in
  let score_map = GG.scores_from_mines hstate.initial_state.dist_map in
  let cmaps = H.create ~size:punters () in
  let sum ints = List.fold_left ~init:0 ~f:(+) ints in
  let acquire_for ~punterid ~source ~target =
    let cmap = H.find_or_add cmaps punterid ~default:(fun _ -> CG.init map) in
    CG.claim_edge cmap source target
  in
  List.iter moves_rev ~f:(GM.iter_move ~f:acquire_for);
  List.map (H.keys cmaps)
    ~f:(fun punter ->
      let cmap = H.find_exn cmaps punter in
      let score = 
        cmap.CG.components
        |> HSet.to_list
        |> List.map
            ~f:(fun comp ->
            List.cartesian_product (CG.mines_of_component cmap comp) (CG.component_vertices cmap comp)
            |> List.map ~f:(H.find_exn score_map)
            |> sum
            )
        |> sum
      in
      (punter, score)
    )

let print_scores scores =
  List.iter scores ~f:(fun (punter, score) ->
    Log.log "punter %d's score is %d\n" punter score)

module type AI = sig
  type state

  val setup           : istate -> state
  val stop            : (int * int) list -> unit

  val choose_move     : state -> hstate -> GM.move
  val opponent_moved  : GM.move -> state -> unit
  val we_moved        : GM.move -> state -> unit
  
  val name            : state -> string
end
