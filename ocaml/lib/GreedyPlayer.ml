open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G
module H = Hashtbl.Poly
module CG = ComponentGraph

type state = {
  punter: GM.punterid;
  punters: int;
  mines: G.V.t list;
  score_map: GG.mine_vertex_map;
  component_map: CG.component_map;
}

let name _ = "GreedyPunter"

let score_of_connecting_to_vertex state component_with_mines v =
  List.sum (module Int : Commutative_group.S with type t = int)
    (CG.mines_of_component state.component_map component_with_mines)
    ~f:(fun mine ->
            match H.find state.score_map (mine, v) with
            | Some s -> s
            | None -> 0 (* impossible *)
        )

(* One-directional. Run in both directions for the full picture. *)
let score_of_connecting_to_vertices state
      component_with_mines
      component_with_vertices =
  List.sum (module Int : Commutative_group.S with type t = int)
    (CG.component_vertices state.component_map component_with_vertices)
    ~f:(score_of_connecting_to_vertex state component_with_mines)

(* Select our next move as an edge in state.map.graph.
 * May update the game state but should not update the 
 * game state to reflect this move.
 *)
let choose_move (state : state) hstate : GM.move =
  let edge_scores =
    let t = H.create ~size:1000 () in
    CG.HSet.iter state.component_map.CG.components ~f:(fun component ->
      let neighbour_edges : G.E.t list =
        ComponentGraph.component_neighbor_edges
          ~allow_options:false
          state.component_map component
      in
      List.iter neighbour_edges ~f:(fun (source, target) ->
        (* `source` is in `component`, target is outside *)
        let score_delta =
          match CG.get_component state.component_map target with
          | None ->
              score_of_connecting_to_vertex state component target
          | Some component2 ->
              (* This move would connect `component` to `component2`. That's
              * usually great, especially if both components contain mines. *)
              score_of_connecting_to_vertices state component component2 +
              score_of_connecting_to_vertices state component2 component
        in
        H.update t (component, (source, target))
          (fun v -> score_delta + Option.value ~default:0 v)
      );
    );
    t
  in
  H.to_alist edge_scores
  |> MyUtil.list_all_max_rev ~cmp:(fun (_, score1) (_, score2) ->
        score1 - score2
      )
  |> Fn.flip List.take 10 (* at most 10 candidates *) (* TODO: random *)
  |> List.map ~f:Tuple2.get1
  |> function
  | [] ->
      GM.Pass { punterid = state.punter }
  | [(_component, (source, target))] ->
      GM.Claim { punterid = state.punter; source; target }
  | (_::_) as candidates ->
      (* When there are multiple candidates, the following algorithm breaks
       * ties.
       * - Parallel flood fill from all candidates on the graph of only free
       *   edges.
       * - Pick edge to each vertex. This can be ambiguous, and we don't do
       *   anything smart about that. The candidate is now associated with _one_
       *   component.
       * - Find score for each vertex in a candidate's cover, from the
       *   perspective of that candidate (each mine of component).
       * - Pick a candidate with a vertex of the highest score.
       *)
      let source_and_component_of_target = Hashtbl.Poly.create () in
      List.iter candidates ~f:(fun (component, (source, target)) ->
        Hashtbl.Poly.set source_and_component_of_target
          ~key:target ~data:(source, component)
      );
      let vertex_to_nearest_target =
        let targets = List.map candidates ~f:(fun (_, (_,target)) -> target) in
        let initial_map = hstate.Player.initial_state.Player.map in
        (* TODO: we could remove edges incrementally instead of all at once *)
        let removed_edges = ref [] in
        List.iter hstate.Player.moves_rev ~f:(
          GM.iter_move ~f:(fun ~punterid ~source ~target ->
              removed_edges := (source, target) :: !removed_edges
          )
        );
        GameGraphUtil.multi_source_bfs
          ~sources:targets
          ~ignored_edges:!removed_edges
          initial_map.GG.graph
      in
      let candidates_with_scores =
        Hashtbl.Poly.to_alist vertex_to_nearest_target
        |> List.map ~f:(fun (v, target) ->
            let (source, component) =
              Hashtbl.Poly.find_exn source_and_component_of_target target
            in
            let mines =
              ComponentGraph.mines_of_component state.component_map component
            in
            let v_score_wrt_component =
              List.sum (module Int : Commutative_group.S with type t = int) mines
                ~f:(fun mine -> Hashtbl.Poly.find_exn state.score_map (mine, v))
            in
            (v, (source, target), component, v_score_wrt_component)
          )
      in
      Log.log "GreedyPlayer: Had %d candidates:\n" (List.length candidates);
(*
      List.iter candidates_with_scores
        ~f:(fun (v, (source, target), _, score) ->
          Log.log "  %d has score %d wrt. %d--%d\n" v score source target
        );
*)
      let (v, (source, target), _, score) =
        Option.value_exn (List.max_elt
            ~cmp:(fun (_,_,_,s1) (_,_,_,s2) -> s1 - s2)
            candidates_with_scores)
      in
      Log.log ("GreedyPlayer: breaking tie by aiming for %d " ^^
                "with potential score %d\n") v score;
      GM.Claim { punterid = state.punter; source; target }


let setup (initial_state : Player.istate) : state =
  let { Player. punter; punters; map } = initial_state in
  let score_map = GG.scores_from_mines initial_state.Player.dist_map in
  let component_map = ComponentGraph.init map in
  { punter; punters; mines=map.GG.mines; score_map; component_map }

let stop _ = ()

let we_moved (move : GM.move) (state : state) =
  GM.iter_move move ~f:(fun ~punterid ~source ~target ->
    CG.claim_edge state.component_map source target
    )

let opponent_moved (move : GM.move) (state : state) =
  GM.iter_move move ~f:(fun ~punterid ~source ~target ->
      CG.opponent_acquired_edge state.component_map source target
    )
