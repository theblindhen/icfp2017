open Core

module G = IntGraph.G
module GM = GameMessages
module H = Hashtbl.Poly

type map = { graph: G.t ; mines: G.V.t list }

(* Load a map from a map message *)
let load_map (map : GameMessages.map) =
  let g = G.create () in
  let { GameMessages. sites; rivers; mines} = map in
    let mine_ids = Hash_set.Poly.of_list mines in
    let mine_vertices = 
      List.fold_left sites ~init:[] ~f:(fun mine_vertices site ->
          let v = G.V.create site in
          G.add_vertex g v ;
          if Hash_set.Poly.mem mine_ids site then
            v :: mine_vertices
          else
            mine_vertices
        ) in
    List.iter ~f:(fun (src, dst) ->
      let src_n = G.V.create src in
      let dst_n = G.V.create dst in
        G.add_edge g src_n dst_n) rivers;
    { graph = g ; mines = mine_vertices }

(* Remove edge claimed by given move *)
let remove_claimed_edge graph move =
  GM.iter_move move
    ~f:(fun ~punterid ~source ~target -> G.remove_edge graph source target)

let copy_map { graph; mines } = { graph = G.copy graph; mines }

module C = Graph.Oper.Choose(G)

let random_edge g =
  C.choose_edge g

(* Contracts the edge e of the graph and returns the removed vertex *)
let contract (g : G.t) (e : G.E.t) =
  let v1, v2 = G.E.src e, G.E.dst e in
  G.succ g v1 |> List.iter ~f:(fun u -> if u <> v2 then G.add_edge g v2 u) ;
  G.remove_vertex g v1 ;
  v1

let one_to_all_shortest_path (g : G.t) (v : G.V.t) =
  let visited = Hash_set.Poly.create ~size:97 () in
  let dists = H.create ~size:97 () in
  let q = Caml.Queue.create () in
  (* invariant: [h] contains exactly the vertices which have been pushed *)
  let push d v =
    if not (Hash_set.Poly.mem visited v) then begin
      Hash_set.Poly.add visited v;
      Caml.Queue.add (v,d) q
    end
  in
  push 0 v;
  let rec loop () =
    if not (Caml.Queue.is_empty q) then
      let v,d  = Caml.Queue.pop q in
      H.set dists ~key:v ~data:d;
      G.iter_succ (push (d+1)) g v;
      loop ()
    else
      ()
  in
  loop ();
  dists

(* Map from (mine * site) to int/score *)
type mine_vertex_map = (G.V.t * G.V.t, int) H.t

(* Returns distance to mines map.
   Note that there is *NO* mapping for a pair (mine, vertex) if there is no
   path between them.
 *)
let distance_to_mine { graph; mines } : mine_vertex_map =
  let result = H.create ~size:(List.length mines * G.nb_vertex graph) () in
  List.iter mines ~f:(fun mine ->
    one_to_all_shortest_path graph mine
    |> H.iteri ~f:(fun ~key:site ~data:dist ->
           H.set result ~key:(mine, site) ~data:dist
    )
  );
  result

(* Returns map from (mine -> site) to score, computed from distance_to_mine map *)
let scores_from_mines (dist_map : mine_vertex_map) : mine_vertex_map =
  dist_map |> H.map ~f:(fun dist -> dist*dist)

let map_to_game_message (map : map) : GM.map =
  { GM.
    sites = G.fold_vertex  (fun v acc -> v::acc) map.graph [] ;
    rivers = G.fold_edges (fun src dst acc -> (src,dst)::acc) map.graph [] ;
    mines = map.mines }


(* Strings for debugging *)

let edge_to_string (e : G.E.t) =
  Printf.sprintf "(%d-%d)" (G.E.src e) (G.E.dst e)

let path_to_string (path : G.E.t list) : string =
  "[ "^ (path |> List.map ~f:edge_to_string |> String.concat ~sep:"; ") ^" ]"



type claims = (GM.punterid, G.E.t list) H.t

(* Displaying a claim map *)
let dot_mine_color = 0x00AA00
let dot_component_color = 0xAA0000
let dot_punter_colors =
    [| 0xb20000; 0x00b247;
       0xb26b00; 0x0047b2;
       0x8eb200; 0x2300b2;
       0x23b200; 0x8e00b2; |] 
let dot_claims_map (oc : Caml.out_channel) (map : map) (claims : claims) (message : string) =
  let mines = Hash_set.Poly.of_list map.mines in
  let edge_to_punter = H.create ~size:97 () in
  H.iteri claims ~f:(fun ~key:punter ~data:edges ->
    List.iter edges ~f:(fun (v1, v2) ->
      H.add_multi edge_to_punter (v1, v2) punter;
      H.add_multi edge_to_punter (v2, v1) punter; (* reversed *)
    )
  );
  let module DisplayClaimsMap = struct
    include G
    let vertex_name v = string_of_int (V.label v)
    let graph_attributes _ = [`HtmlLabel message]
    let default_vertex_attributes _ = []
    let vertex_attributes v =
      if Hash_set.Poly.mem mines v then
        [ `Color dot_mine_color ; `Shape `Diamond ]
      else
        [ ]
    let default_edge_attributes _ = []
    let edge_attributes e =
      match H.find edge_to_punter e with
      | None | Some [] -> [ `Color 0xBBBBBB ]
      | Some [p] -> [ `Color dot_punter_colors.(p); `Penwidth 4.0 ]
      | Some [p1;p2] ->  [ `Color dot_punter_colors.(p1); `Penwidth 6.0; `Style `Dotted;
                      `Label (Printf.sprintf "o:%d" p2); `Fontcolor dot_punter_colors.(p2) ]
      | _ -> failwith "More than two punters on an edge?"
    let get_subgraph _ = None
  end in
  let module Neato = Graph.Graphviz.Neato(DisplayClaimsMap) in
  Neato.output_graph oc map.graph


let display_claims_map (map : map) (claims : claims) (message : string) =
  MyUtil.display_dot_with (fun oc -> dot_claims_map oc map claims message)
