open Core
open Graph

module GG = GameGraph
module G = GG.G
module HSet = Hash_set.Poly

type component =
  C of int

type component_map =
  { (* The graph with added edges from "component vertices" to covered vertices. *)
    graph : G.t ; 
    mines : G.V.t HSet.t ;
    claimed_edges: G.E.t HSet.t ;
    (* The component vertices (their labels).
       There is an edge from a component vertex to each vertex of that component *)
    components : component HSet.t ;
    opponent_edges : G.E.t HSet.t ;
    opponent_opt_edges : G.E.t HSet.t ;
    mutable max_vertex : int ;
  }

let log_component_map map =
  Log.log "  logging component map:\n";
  G.iter_edges
    (fun src dst ->
      Log.log "  - %d -> %d\n" src dst)
    map.graph;
  HSet.iter ~f:(fun (C comp) ->
    Log.log "  - %d is a component\n" comp)
    map.components

let init (map : GG.map) : component_map =
  let gg = G.copy map.GG.graph in
  let max_vertex = G.fold_vertex (fun v m -> max v m) (map.GG.graph) Caml.min_int in
  let claimed_edges = HSet.create ~size:20 () in
  let components = HSet.create ~size:20 () in
  let max_vertex =
    List.fold_left map.GG.mines ~init:(max_vertex + 1) ~f:(fun counter mine ->
      let v = G.V.create counter in
      G.add_edge gg v mine;
      HSet.add components (C v);
      counter+1
    )  in
  { graph = gg ;
    mines = HSet.of_list map.GG.mines ;
    claimed_edges = claimed_edges ;
    components = components ;
    opponent_edges = HSet.create () ;
    opponent_opt_edges = HSet.create () ;
    max_vertex = max_vertex ;
  }

let get_component (cg : component_map) (v : G.V.t) : component option =
  let cv = 
    G.succ cg.graph v
    |> List.find ~f:(fun u -> HSet.mem cg.components (C u))
  in
  match cv with
  | Some v -> Some (C v)
  | None -> None

let claim_edge (cg : component_map) (v1 : G.V.t) (v2 : G.V.t) =
  HSet.add cg.claimed_edges (G.E.create v1 () v2) ;
  HSet.add cg.claimed_edges (G.E.create v2 () v1) ;
  let absorb_vertex component v =
    (* Log.log "CG absorbing vertex %d into component %d\n" v component; *)
    G.add_edge cg.graph component v
    in 
  match (get_component cg v1, get_component cg v2) with
  | (None, None) ->
      (* Edge in nowhere : Create a new component for this edge *)
      let cv = G.V.create cg.max_vertex in
      cg.max_vertex <- cg.max_vertex + 1;
      HSet.add cg.components (C cv);
      (* Log.log "CG creating new component %d to vertex %d\n" cv v1; *)
      G.add_edge cg.graph v1 cv;
      G.add_edge cg.graph v2 cv
  | (Some (C c), None) -> absorb_vertex c v2
  | (None, Some (C c)) -> absorb_vertex c v1
  | (Some (C c1), Some C c2) ->
     if c1 <> c2 then
       begin
       (* Merge the two components:
          - We move all edges from c2 to c1.
          - We then remove all edges from c2.
          - We then remove c2 from the cg.components.
          - But we *KEEP* c2 in the graph for efficency ! *)
         List.iter (G.succ cg.graph c2) ~f:(G.add_edge cg.graph c1) ;
         List.iter (G.succ_e cg.graph c2) ~f:(G.remove_edge_e cg.graph) ;
         (* Log.log "CG collapsing components %d and %d\n" c1 c2; *)
         HSet.remove cg.components (C c2)
       end

let opponent_acquired_edge (cg : component_map) (source: G.V.t) (target: G.V.t) : unit =
  (* G.remove_edge cg.graph source target *)
  let edge1 = G.E.create source () target in
  let edge2 = G.E.create target () source
  in
  if HSet.mem cg.opponent_edges edge1 then
    begin
    HSet.add cg.opponent_opt_edges edge1;
    HSet.add cg.opponent_opt_edges edge2
    end
  else
    begin
    HSet.add cg.opponent_edges edge1;
    HSet.add cg.opponent_edges edge2
    end

(* The list of vertices of a component, without the component vertex itself *)
let component_vertices (cg : component_map) (C component : component) : G.V.t list =
  G.succ cg.graph component

(* The list of vertices directly reachable from a component, without the component vertices themselves *)
let component_neighbor_edges ~(allow_options:bool) (cg : component_map) (component : component) : G.E.t list =
  let C cv = component in
  let cverts = cv::(component_vertices cg component) |> HSet.of_list in
  let nset = HSet.create () in
  cverts
  |> HSet.iter ~f:(fun v ->
         G.succ_e cg.graph v
         |> List.iter ~f:(fun (src,dst) ->
                let src, dst = if HSet.mem cverts src then src, dst else dst, src in
                if not (HSet.mem cverts dst) then
                  if (not (HSet.mem cg.opponent_edges (src,dst)))
                     || (allow_options && (not (HSet.mem cg.opponent_opt_edges (src,dst)))) then
                    HSet.add nset (src, dst)
       )) ;
  HSet.to_list nset

let mines_of_component (cg : component_map) (component : component) : G.V.t list =
  component_vertices cg component
  |> List.filter ~f:(HSet.mem cg.mines)



let infinite_weight = 10000000.
module type IsClaimed = sig
  val option_weight : float
  val is_claimed : G.E.t -> bool
  val is_opponent : G.E.t -> bool
  val is_already_optioned : G.E.t -> bool
end
module ShortestPathModule (IsClaimed : IsClaimed) =
  Path.Dijkstra(G)(
    struct
        type edge = G.E.t
        type t = float
        let weight e =
          if IsClaimed.is_claimed e then
            float 0
          else if IsClaimed.is_opponent e then
            if IsClaimed.is_already_optioned e then
              infinite_weight
            else
              IsClaimed.option_weight
          else
            float 1 
        let compare = Float.compare
        let add = (+.)
        let zero = float 0
    end)

type path_result =
  | EmptyPath
  | Path of G.E.t list * float
  | TooManyOptions
  | NoPath
(* A list of edges which, if claimed (or optioned), will connect a component to a vertex.
   The list of edges need not be contiguous, if an in-between edge has already
   been claimed.
   Raises Not_found if there is no such path.
 *)
let _path_cvertex_to_cvertex ~(rem_options:int) ~(option_weight: float) (cg : component_map) ~(source: G.V.t) ~(target: G.V.t) : path_result =
  let interpret_path (cpath, weight) =
    if weight >= infinite_weight then NoPath
    else
        match cpath with
        | []  -> EmptyPath
        | p ->
           let drop_last ls =
             match List.rev ls with
             | [] -> failwith "CG Dropping last of an empty list"
             | _::body  -> List.rev body in
           let p = 
             match (p, HSet.mem cg.components (C source), HSet.mem cg.components (C target)) with
             | (p,     false, false) -> p
             | (_::tl,  true, false) -> tl
             | (p,     false,  true) -> drop_last p
             | (_::tl,  true,  true) -> drop_last tl
             | ([], true, _) -> failwith "CG Tailing an empty list"
           in
           let edges_to_acq = p |> List.filter ~f:(fun e -> not (HSet.mem cg.claimed_edges e)) in
           let option_edges = edges_to_acq |> List.filter ~f:(HSet.mem cg.opponent_edges)
           in
           if List.length option_edges > rem_options then
             TooManyOptions
           else
             if edges_to_acq = [] then
               EmptyPath
             else
                Path (edges_to_acq, weight)
    in
  let module Dijkstra =
    ShortestPathModule(struct
        let option_weight = if rem_options = 0 then infinite_weight else option_weight
        let is_claimed e = HSet.mem cg.claimed_edges e
        let is_opponent e = HSet.mem cg.opponent_edges e
        let is_already_optioned e = HSet.mem cg.opponent_opt_edges e
      end
      ) in
  Dijkstra.shortest_path (cg.graph) source target |> interpret_path

let path_component_to_vertex ~(rem_options:int) ~(option_weight:float) (cg : component_map) (C component : component) (v : G.V.t) : (G.E.t list * float) =
  match _path_cvertex_to_cvertex ~rem_options:rem_options ~option_weight:option_weight cg ~source:component ~target:v with
  | NoPath -> raise Not_found
  | EmptyPath -> [], 0.
  | Path (p, weight) -> p, weight
  | TooManyOptions -> raise Not_found (* TODO: Do better? *)

let path_component_to_component ~(rem_options:int) ~(option_weight:float) (cg : component_map) (C source : component) (C target : component) : (G.E.t list * float) =
  match _path_cvertex_to_cvertex ~rem_options:rem_options ~option_weight:option_weight cg ~source:source ~target:target with
  | NoPath -> raise Not_found
  | EmptyPath -> [], 0.
  | Path (p, weight) -> p, weight
  | TooManyOptions -> raise Not_found (* TODO: Do better? *)


(* Displaying a component map *)
let dot_mine_color = 0x00AA00
let dot_component_color = 0xAA0000
let dot_claimed_color = 0xb20000
let dot_output_map (oc : Caml.out_channel) (cg : component_map) =
  let components = cg.components in
  let module DisplayComponentMap = struct
    include GG.G
    let vertex_name v = string_of_int v
    let graph_attributes _ = []
    let default_vertex_attributes _ = []
    let vertex_attributes v =
      if Hash_set.Poly.mem cg.mines v then
        [ `Color dot_mine_color ; `Shape `Diamond ]
      else if HSet.mem components (C v) then
        [ `Color dot_component_color ; `Shape `Doubleoctagon ]
      else
        [ ]
    let default_edge_attributes _ = []
    let edge_attributes e =
      if HSet.mem cg.claimed_edges e then
        [ `Color dot_claimed_color; `Penwidth 4.0 ]
      else
        [ `Color 0xBBBBBB ]
    let get_subgraph _ = None
  end in
  let module Neato = Graphviz.Neato(DisplayComponentMap) in
  Neato.output_graph oc cg.graph

let display_with (cg : component_map) =
  MyUtil.display_dot_with (fun oc -> dot_output_map oc cg)
