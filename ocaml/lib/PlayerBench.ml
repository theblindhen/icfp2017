
open Core
module GM = GameMessages

type player = {
  choose_move     : Player.hstate -> GM.move;
  we_moved        : GM.move -> unit;
  opponent_moved  : GM.move -> unit
}

module MaxScore = struct

  let target hstate =
    match
      Player.compute_scores hstate
      |> List.filter ~f:(fun (id, _) -> id <> (Player.punter hstate))
      |> List.max_elt ~cmp:(fun (_, s1) (_, s2) -> s1 - s2)
    with
    | None -> failwith "No opponents?"
    | Some (id, _) -> id

end

module PlayerBelowUs = struct

  let target hstate =
    let scores = Player.compute_scores hstate in
    Player.print_scores scores;
    let our_score = 
      match List.Assoc.find ~equal:(=) scores (Player.punter hstate) with
      | None -> 0
      | Some n -> n 
    in
    let below, above = List.partition_tf scores
      ~f:(fun (_, their_score) -> their_score < our_score) in
    let candidates_below =
      List.filter below ~f:(fun (_, their_score) -> their_score > (our_score / 2))
      |> List.sort ~cmp:(fun (_, s1) (_, s2) -> s2 - s1)
    in 
    let candidates_above =
      List.filter above ~f:(fun (_, their_score) -> their_score < our_score * 2)
      |> List.sort ~cmp:(fun (_, s1) (_, s2) -> s1 - s2)
    in
    let candidates = 
      candidates_below @ candidates_above
      |> List.map ~f:fst
      (*(fun (id, score) -> Log.log "%d: %d\n" id score; id)*)
    in
      match List.hd candidates with
      | None -> Player.punter hstate
      | Some id -> id

end

module StandardPlayer =
  PlayerComb.LoggingPlayer(
    PlayerComb.LeftBiasSwitch(MineConnectingPlayer)(
      PlayerComb.LeftBiasSwitch(GreedyPlayer)(
        PlayerComb.SplitPersonalityPlayer(GreedyPlayer)(PlayerBelowUs))))

module EarlySabotagePlayer =
  PlayerComb.ImpersonatingPlayer(GreedyPlayer)(MaxScore)

module SplitPersonalitySabotagePlayer =
  PlayerComb.SplitPersonalityPlayer(GreedyPlayer)(MaxScore)

module GreedyOnly =
    PlayerComb.LeftBiasSwitch(GreedyPlayer)(
      PlayerComb.SplitPersonalityPlayer(GreedyPlayer)(PlayerBelowUs))

let players = [
  "early_sabotage", (module EarlySabotagePlayer : Player.AI);
  "standard", (module StandardPlayer : Player.AI);
  "random", (module PlayerComb.LoggingPlayer(RandomPlayer) : Player.AI);
  "random_splurge", (module RandomSplurgePlayer : Player.AI);
  "split_sabotage", (module SplitPersonalitySabotagePlayer : Player.AI);
  "pass", (module PassPlayer : Player.AI);
  "greedy_only", (module GreedyOnly : Player.AI);
]

let init_player (ai : (module Player.AI)) istate =
  let module P = (val ai : Player.AI) in
  let s = P.setup istate in 
  { 
    choose_move    = P.choose_move s;
    we_moved       = (fun m -> P.we_moved m s);
    opponent_moved = (fun m -> P.opponent_moved m s) 
  }

let lookup_player name =
  match List.Assoc.find ~equal:(=) players name with
  | None -> failwith "player not found"
  | Some ai -> ai
