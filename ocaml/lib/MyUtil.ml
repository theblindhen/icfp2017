open Core

(* Returns all elements of max size from a totally-ordered list `l`, reversed.
 *)
let list_all_max_rev ~cmp l =
  let rec loop ~acc l =
    match acc, l with
    | acc, [] -> acc
    | [], h::t -> loop ~acc:[h] t
    | acc_h::acc_t, h::t ->
        match cmp h acc_h with
        | 0 ->
            loop ~acc:(h::acc) t (* add h to acc *)
        | n when n > 0 ->
            loop ~acc:[h] t      (* discard acc and start a new one *)
        | _ (* n when n < 0 *) ->
            loop ~acc t          (* discard h and keep going *)
  in
  loop ~acc:[] l

(* See list_all_max_rev *)
let list_all_min_rev ~cmp =
  list_all_max_rev ~cmp:(fun a b -> cmp b a)

let compile_dot =
  match Sys.getenv "ONLY_DOT" with
  | None -> true
  | Some _ -> false

let display_dot_with ~(out_file:string) (dotgen: Caml.out_channel -> unit) =
  if compile_dot then
    let tmp = Filename.temp_file "graph" ".dot" in
    let oc = Caml.open_out tmp in
    dotgen oc;
    ignore (Sys.command ("dot -Tpng -o"^ out_file ^".png " ^ tmp));
    Caml.close_out oc;
    Sys.remove tmp
  else
    let oc = Caml.open_out (out_file ^ ".dot") in
    dotgen oc;
    Caml.close_out oc
