
module GM = GameMessages

type state = unit

let name _ = "Pass"

let setup _ = ()
let stop _ = ()

let choose_move _ hstate = GM.Pass { punterid = (Player.punter hstate) }

let we_moved _ _ = ()
let opponent_moved _ _ = ()

