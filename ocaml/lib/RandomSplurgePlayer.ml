
open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G

type state = {
  map: GG.map;
  mutable skipped : bool
}

let name _ = "RandomSplurgePlayer"

let setup istate : state = { 
  map = GG.copy_map istate.Player.map;
  skipped = false
}

let stop _ = ()

let random_path graph =
  let edgeA = GG.random_edge graph in
  let src = G.E.src edgeA in
  let mid = G.E.dst edgeA in
  let edgeB = 
   G.succ_e graph mid
   |> List.find_exn ~f:(fun edgeB -> edgeB <> edgeA && G.E.src edgeB = mid && G.E.dst edgeB <> src) 
  in (src, mid, G.E.dst edgeB)

let choose_move state hstate : GM.move =
  let punterid = Player.punter hstate in
  if state.skipped then begin
    try
      let (src, mid, dst) = random_path state.map.GG.graph in
        state.skipped <- false;
        GM.Splurge { 
          punterid;
          start = src;
          path = [mid; dst]
        }
    with _ -> GM.Pass { punterid }
  end else begin
    state.skipped <- true;
    GM.Pass { punterid }
  end

let we_moved (move : GM.move) ({ map } : state) =
  Log.log "we_moved: %s\n" (GM.string_of_move move);
  GG.remove_claimed_edge map.GG.graph move

let opponent_moved (move : GM.move) ({ map } : state) =
  Log.log "opponent_moved: %s\n" (GM.string_of_move move);
  GG.remove_claimed_edge map.GG.graph move
