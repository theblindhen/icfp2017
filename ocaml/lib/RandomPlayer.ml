module GM = GameMessages
module GG = GameGraph
module G = GG.G

type state = {
  map: GG.map;
}

let name _ = "RandomPlayer"

let setup istate : state =
  { map = GG.copy_map istate.Player.map }

let stop _ = ()

let choose_move state hstate : GM.move =
  let graph = state.map.GG.graph in
  let edge = GG.random_edge graph in
  let src = G.E.src edge in
  let dst = G.E.dst edge in
  GM.Claim { punterid = Player.punter hstate; source = src; target = dst }

let we_moved (move : GM.move) ({ map } : state) =
  GG.remove_claimed_edge map.GG.graph move

let opponent_moved (move : GM.move) ({ map } : state) =
  GG.remove_claimed_edge map.GG.graph move

