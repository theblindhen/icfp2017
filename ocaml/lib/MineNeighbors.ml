open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G
module H = Hashtbl.Poly


type mine_neighbors = G.t

let mine_neighbors ~(map:GG.map) ~(dist_map:GG.mine_vertex_map) ~(max_edges:int) : mine_neighbors =
  let mines = map.GG.mines in
  let mine_neighbors = G.create () in
  List.iter mines ~f:(fun mine -> G.add_vertex mine_neighbors mine);
  let neighbor_ratio = H.create () in
  map.GG.graph
  |> G.iter_vertex
       (fun vertex ->
         let dists = List.map mines ~f:(fun mine ->
                         match H.find dist_map (mine, vertex) with
                         | None -> None
                         | Some d -> Some (mine,d))
                     |> List.filter_opt
                     |> List.sort ~cmp:(fun (_,d1) (_,d2) -> Int.compare d1 d2)
         in
         match dists with
         | [] -> ()
         | (min_mine, min_dist)::tail ->
            List.iter tail ~f:(fun (mine, dist) ->
                let mine_pair = if min_mine < mine then(min_mine, mine) else (mine, min_mine) in
                let ratio = (float (dist-1)) /. (float min_dist) in
                let is_better = match H.find neighbor_ratio mine_pair with
                                | None -> true
                                | Some r -> ratio < r
                in
                if is_better then H.set neighbor_ratio ~key:mine_pair ~data:ratio
              ) 
       );
  let mine_pairs =
    neighbor_ratio
    |> H.to_alist
    |> List.sort ~cmp:(fun (_,r1) (_,r2) -> Float.compare r1 r2)
    |> List.unzip
    |> fst  in
  List.fold_left mine_pairs ~init:0 ~f:(fun edges_added (mine1,mine2) ->
      if edges_added < max_edges then
        begin
        G.add_edge mine_neighbors mine1 mine2 ;
        edges_added + 1
        end
      else
        edges_added
    ) |> ignore ;
  mine_neighbors
