open Core
open Unix

let log_file =
  Sys.getenv "LOG_FILE"
  |> Option.map ~f:(Out_channel.create ~append:true)

let log fmt =
  let time = Time.format (Time.now ()) "%H:%M:%S" ~zone:Time.Zone.utc in
  match log_file with
  | None -> Printf.eprintf ("[%s] " ^^ fmt ^^ "%!") time
  | Some channel -> Printf.fprintf channel ("[%s] " ^^ fmt ^^ "%!") time
