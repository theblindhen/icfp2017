open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G
module HSet = Hash_set.Poly
module CG = ComponentGraph

type state = {
  component_map: CG.component_map;
}

let name _ = "MineConnectingPlayer"

let log_paths paths =
  List.iter ~f:(fun (CG.C src, CG.C dst, path, length) -> 
    Log.log "  %d -> %d (length %d)\n" src dst length) 
    paths;
    paths

let log_components components =
  HSet.iter ~f:(fun (CG.C c) ->
    Log.log "  %d is a component\n" c) components

(* Returns the number of edges adjacent to this component. *)
let component_connectivity state component =
  List.length (CG.component_neighbor_edges ~allow_options:false state.component_map component)

let choose_move (state : state) hstate : GM.move =
  Log.log "Player %d to choose. %d options remaining\n" hstate.Player.initial_state.Player.punter hstate.Player.rem_options ;
  let component_map = state.component_map in
  let punterid = Player.punter hstate in
  (*log_components components;*)
  let component_neighbors = HSet.create () in
  hstate.Player.initial_state.Player.mine_neighbors
  |> G.iter_edges (fun m1 m2 ->
         match CG.get_component component_map m1, CG.get_component component_map m2 with
         | Some c1, Some c2 ->
            let cpair = if c1 < c2 then (c1,c2) else (c2,c1) in
            HSet.add component_neighbors cpair
         | _,_ ->
            Log.log "MineConnectingPlayer: Missing component for mine %d or %d\n" m1 m2
       ) ;
  HSet.fold component_neighbors
    ~init:[]
    ~f:(fun acc (src_comp, dst_comp) ->
      try
        let spath, weight = 
          CG.path_component_to_component ~rem_options:hstate.Player.rem_options ~option_weight:2.5 state.component_map src_comp dst_comp 
        in
        if weight >. 0. then
          (src_comp, dst_comp, spath, weight)::acc
        else acc
      with Not_found -> acc
    ) 
  (*|> log_paths*)
  |> MyUtil.list_all_min_rev
      ~cmp:(fun (_,_,_,w1) (_,_,_,w2) -> Float.compare w1 w2)
  (* prioritize components that when combined contains the largest number of mines *)
  |> MyUtil.list_all_max_rev
      ~cmp:(fun (src_comp1, dst_comp1, _, _) (src_comp2, dst_comp2, _, _) ->
        let size comp = List.length (CG.mines_of_component component_map comp) in
        Int.compare (size src_comp1 + size dst_comp1) (size src_comp2 + size dst_comp2))
  (* Each pair of components we could connect adds two candidates for what edge
   * we should pick: the first edge of the discovered path or the last. *)
  |> List.concat_map ~f:(fun (src_comp, dst_comp, path, _) ->
        [ (src_comp, List.hd_exn path);
          (dst_comp, List.last_exn path) ]
      )
  (* Pick the edge leaving the "smallest" component. This will usually be the
   * component that is most difficult to connect to later. *)
  |> List.min_elt ~cmp:(fun (component1, _) (component2, _) ->
        component_connectivity state component1 -
        component_connectivity state component2
      )
  |> function
     | None -> GM.Pass { punterid }
     | Some (_, (source, target)) ->
        let edge = (G.E.create source () target) in
        if HSet.mem state.component_map.CG.opponent_edges edge then
          begin
          if HSet.mem state.component_map.CG.opponent_opt_edges edge then
            (* TODO: Return Pass or something? *)
            failwith "MineConnectingPlayer: Trying to option an already optioned edge!"
          else
            GM.Option { punterid; source; target }
          end
        else
          GM.Claim { punterid; source; target }
  
let setup (initial_state : Player.istate) : state =
  let { Player. punter; punters; map } = initial_state in
  let component_map = CG.init map in
    { component_map }

let stop _ = ()

let we_moved (move : GM.move) (state : state) =
  let acquire source target = 
    CG.claim_edge state.component_map source target
  in
  GM.iter_move move ~f:(fun ~punterid ~source ~target ->
        acquire source target
    )

let opponent_moved (move : GM.move) (state : state) =
  GM.iter_move move ~f:(fun ~punterid ~source ~target -> 
      CG.opponent_acquired_edge state.component_map source target
    )

