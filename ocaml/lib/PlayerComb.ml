
open Core
open Player

module GM = GameMessages

module LeftBiasSwitch(P1 : AI)(P2 : AI) : AI = struct

  (* TODO: can be optimized to drop P1.state once we switch to right side. *)
  type switch = L | R 

  type state = (P1.state * P2.state * switch ref)

  let setup istate =
    (P1.setup istate, P2.setup istate, ref L)

  let stop scores = P1.stop scores; P2.stop scores

  let choose_move (s1, s2, switch) hstate =
    match !switch with
    | L ->
        begin match P1.choose_move s1 hstate with
        | GM.Pass _ ->
            Log.log "LeftBiasSwitch: switching to %s player\n" (P2.name s2);
            switch := R;
            P2.choose_move s2 hstate
        | move -> move
        end
    | R -> P2.choose_move s2 hstate

  let opponent_moved move (s1, s2, switch) =
    P1.opponent_moved move s1;
    P2.opponent_moved move s2

  let we_moved move (s1, s2, switch) =
    P1.we_moved move s1;
    P2.we_moved move s2

  let name (s1, s2, switch) =
    match !switch with
    | L -> P1.name s1
    | R -> P2.name s2

end

let replay_setup setup we_moved opponent_moved istate moves_rev = 
  let s = setup istate in
  List.iter (List.rev moves_rev)
    ~f:(fun move ->
         if GM.move_by move = istate.Player.punter then
           we_moved move s
         else
           opponent_moved move s);
  s

(* A wrapper that causes a player to sleep (i.e., not
 * update its internal game state based on moves)
 * until the first time it is asked to choose a move. *)
module SleepingPlayer(P : Player.AI) : Player.AI = struct

  type status = Asleep | Awake of P.state
  type state = status ref

  let setup _ = ref Asleep
  
  let stop scores = P.stop scores

  let we_moved move state =
    match !state with
    | Asleep -> ()
    | Awake s -> P.we_moved move s

  let opponent_moved move state =
    match !state with
    | Asleep -> ()
    | Awake s -> P.opponent_moved move s

  let name state = match !state with
    | Asleep -> "Sleeper (sleeping)"
    | Awake s ->
        Printf.sprintf "Sleeper (awake - %s)" (P.name s)

  let choose_move state hstate =
    match !state with
    | Asleep ->
        let { Player. initial_state } = hstate in
        let s = replay_setup P.setup P.we_moved P.opponent_moved initial_state (hstate.Player.moves_rev) in
        Log.log "SleeperPlayer: awaking %s\n" (P.name s);
        state := (Awake s);
        P.choose_move s hstate
    | Awake s ->
        P.choose_move s hstate

end

module type Scope = sig
  val target : hstate -> GM.punterid
end

let no_options hstate =
  { hstate with Player. rem_options = 0 }

module ImpersonatingPlayer(P : Player.AI)(S : Scope) = struct

  type status = Honest | Dishonest of P.state * GM.punterid
  type state = status ref

  let setup _ = ref Honest
  
  let stop scores = P.stop scores

  let moved move state id =
    if GM.move_by move <> id then
      P.opponent_moved move state
    else
      P.we_moved move state

  let we_moved move state =
    match !state with
    | Honest -> ()
    | Dishonest (s, id) -> moved move s id

  let opponent_moved = we_moved

  let name state = match !state with
    | Honest -> "Inactive impersonator"
    | Dishonest (s, id) ->
        Printf.sprintf "Impersionating %d with %s" id (P.name s)

  let choose_move state hstate =
    let oid = hstate.Player.initial_state.Player.punter in
    match !state with
    | Honest ->
        let id = S.target hstate in
        let hstate = Player.set_punter hstate id in
        let { Player. initial_state; moves_rev } = hstate in
        let s = replay_setup P.setup P.we_moved P.opponent_moved (hstate.Player.initial_state) moves_rev in
        Log.log "Decided to impersonate %d with %s\n" id (P.name s);
        state := Dishonest (s, id);
        GM.set_move_id (P.choose_move s (Player.set_punter (no_options hstate) id)) oid
    | Dishonest (s, id) ->
        GM.set_move_id (P.choose_move s (Player.set_punter (no_options hstate) id)) oid

end

module SplitPersonalityPlayer(P : Player.AI)(S : Scope) = struct

  type state = unit

  let setup _ = ()
  
  let stop scores = P.stop scores

  let we_moved _ _ = ()
  let opponent_moved _ _ = ()

  let name _ = "Split personality"

  let choose_move state hstate =
    let oid = hstate.Player.initial_state.Player.punter in
    let nid = S.target hstate in
    let hstate = Player.set_punter (no_options hstate) nid in
    let { Player. initial_state; moves_rev } = hstate in
    let s = replay_setup P.setup P.we_moved P.opponent_moved initial_state moves_rev in
    Log.log "SplitPersonalityPlayer: decided to target %d with %s\n" nid (P.name s);
    GM.set_move_id (P.choose_move s hstate) oid

end

let game_log =
  Sys.getenv "GAME_LOG_FILE"
  |> Option.map ~f:(Out_channel.create ~append:true)

let png_log_dir = Sys.getenv "PNG_LOG_DIR"

module H = Hashtbl.Poly
module CG = ComponentGraph
module GG = GameGraph
module G = GG.G

module LoggingPlayer(P : Player.AI) : Player.AI = struct

  type state = P.state * (int ref) * GG.map * (int, G.E.t list) H.t

  let log fmt =
    match game_log with
    | None -> Log.log fmt
    | Some channel -> Printf.fprintf channel (fmt ^^ "%!")

  let moved move (state, turn, map, claims) =
    let message = GM.string_of_move move in
    let acquire_for ~punterid ~source ~target =
      H.add_multi claims ~key:punterid ~data:(G.E.create source () target);
    in
    turn := !turn + 1;
    log "turn %d: %s\n" !turn message;
    GM.iter_move move ~f:acquire_for ;
    match png_log_dir with
    | None -> ()
    | Some dir ->
        let out_file = Printf.sprintf "%s/turn%04d" dir !turn in
        GG.display_claims_map ~out_file map claims message
      
  let setup istate =
    log "we are punter %d\n" (istate.Player.punter);
    let claims = H.create ~size:97 () in
    (P.setup istate, ref 0, istate.Player.map, claims)

  let stop scores = 
    List.iter scores
      ~f:(fun (punter, score) -> log "punter %d scored %d\n" punter score)

  let name (state, _, map, _) = P.name state

  let we_moved move (state, turn, map, claims) =
    moved move (state, turn, map, claims);
    P.we_moved move state

  let opponent_moved move (state, turn, map, claims) =
    moved move (state, turn, map, claims);
    P.opponent_moved move state

  let choose_move (state, _, map, _) hstate =
    let move = P.choose_move state hstate in
    log "%s chose: %s\n" (P.name state) (GM.string_of_move move);
    move

end
