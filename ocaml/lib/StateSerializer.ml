open Core

(* Adapter for the refill-and-flush interface of camlzip. *)
let string_through_zlib
    (f: (Caml.Bytes.t -> int) -> (Caml.Bytes.t -> int -> unit) -> unit)
    (input: string) : string =
  let out_buffers_rev : string list ref = ref [] in
  let input_len = String.length input in
  let input_pos = ref 0 in
  let refill buffer =
    let refill_len =
      Int.min (Caml.Bytes.length buffer) (input_len - !input_pos)
    in
    Caml.Bytes.blit_string input !input_pos buffer 0 refill_len;
    input_pos := !input_pos + refill_len;
    refill_len
  in
  let flush buf buf_length =
    out_buffers_rev :=
      Caml.Bytes.sub_string buf 0 buf_length :: !out_buffers_rev
  in
  f refill flush;
  String.concat (List.rev !out_buffers_rev)

let compress : string -> string =
  string_through_zlib
    (Zlib.compress ~level:3 ~header:false)

    (* 16240 *)
let decompress : string -> string =
  string_through_zlib
    (Zlib.uncompress ~header:false)

module Make(T: sig type t end) : sig
  val encode: T.t -> string
  val decode: string -> T.t
end = struct
  let encode state =
    B64.encode (compress (Marshal.to_string state [Marshal.Closures]))

  let decode str =
    Marshal.from_string (decompress (B64.decode str)) 0
end
