The Blind Hen 2017 ICFP Contest submission
==========================================

Team members
------------

Johan Rosenkilde
Jonas B. Jensen
Kasper Svendsen

How our submission works
------------------------

The strategy is in three phases:

1. Attempt to connect all mines into as few components as possible. Prefer to
   connect pairs of mines that are close to each other. Use the Options
   extension if available, where a river that can be taken with an option is
   made to have a higher weight than normal rivers in the shortest-path
   algorithm.

2. When no more mines can be connected, we use a greedy algorithm that always
   chooses the river that gives the highest score in this move. To break ties
   when there are several rivers to choose from, we prefer the river that would
   bring us closest to the most valuable site anywhere on the map.

3. When no further scoring is possible, we start sabotaging other players.
   Instead of using a dedicated algorithm for this, we instantiate our greedy
   algorithm from the previous step from the perspective of another player. We
   let it compute the optimal move from his perspective, and then we make that
   move instead of him.

We ignore the Futures and Splurges extensions. None of them seem useful for
boosting our score more than a few percent on large maps.

We use a data structure that maintains information about connected components
around each mine along rivers we own. We have a heuristic notion of which mines
are "neighbours", and we limit the number of such neighbour pairs to 64 to make
sure we don't waste too much time on mine-pair calculations for large maps.

How to build
------------

First install the "opam" package manager. It's already installed on the contest
VM, and you can also skip the following steps there:

    $ opam init # Answer `y` at the prompt
    $ ocaml -version
    $ opam switch create 4.04.2 # if version wasn't 4.04.x
    follow instructions after previous command if run

Install packages we need:

    $ opam install solvuu-build core async merlin ocamlgraph base64 camlzip

Now build and run this program:

    $ make .merlin native
    $ ./play.native

On subsequent terminal sessions, do the following unless you allowed `opam init`
to modify your shell rc:

    $ eval `opam config env`
