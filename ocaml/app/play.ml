open Core

let () =
  let ai = match Sys.getenv "PLAYER" with
    | None -> (module PlayerBench.StandardPlayer : Player.AI)
    | Some name -> PlayerBench.lookup_player name
  in
    let module P = (val ai : Player.AI) in
    let module OM = OfflineMode.Make(P) in
      OM.entry_point ()
