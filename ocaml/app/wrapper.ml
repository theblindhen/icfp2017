open Core
open Async

let write_str writer who str =
  let str = Printf.sprintf "%d:%s" (String.length str) str in
    printf "%s: %s\n" who str;
    Writer.write writer str

let read_str who reader =
  (Reader.read_until reader (`Char ':') ~keep_delim:false 
  >>| function
  | `Eof -> None
  | `Eof_without_delim s -> Some (int_of_string s)
  | `Ok s -> Some (int_of_string s))
  >>= function
  | None -> Deferred.return None
  | Some n ->
      let str_buffer = String.create n in
      Reader.really_read reader ~len:n str_buffer >>| fun x ->
        printf "%s: %s\n" who str_buffer;
        match x with
        | `Eof _ -> None
        | `Ok    -> Some (str_buffer)

module J = Yojson.Basic.Util

let filter_state json =
  let l = J.to_assoc json in
  let state = J.member "state" json in
  let json = `Assoc (List.filter ~f:(fun (s, _) -> s <> "state") l) in
    (state, Yojson.Basic.to_string json)

let add_state json state = 
  match state with
  | None -> json
  | Some state ->
      let l = J.to_assoc (Yojson.Basic.from_string json) in
      Yojson.Basic.to_string (`Assoc (("state", state)::l))

let run_setup msg state =
  Process.create ~prog:"./play.native" ~args:[] () >>=
  function
  | Ok p ->
      begin
      let writer = Process.stdin p in
      let reader = Process.stdout p in
        write_str writer "P -> O" (add_state msg state);
        begin
        read_str "O -> P" reader >>= function
        | None -> Deferred.return None
        | Some resp ->
            let json = Yojson.Basic.from_string resp in
              Deferred.return (Some (filter_state json))
        end
      end
  | Error e -> Deferred.return None

let rec loop reader writer state =
  read_str "S -> P" reader >>= function
  | None -> Deferred.return None
  | Some msg -> run_setup msg state >>= function
    | None -> Deferred.return None
    | Some (state, resp) ->
        write_str writer "P -> S" resp;
        loop reader writer (Some state)

let main port () =
  let host_and_port = Tcp.to_host_and_port "punter.inf.ed.ac.uk" port in
  Tcp.connect host_and_port >>= fun (_, reader, writer) ->
    write_str writer "P -> S" "{\"me\": \"TheBlindHen\"}";
    read_str "S -> P" reader >>= function
    | None -> Deferred.return None
    | Some _ -> loop reader writer None

let () =
  let port = (int_of_string (Sys.argv.(1))) in
  match Thread_safe.block_on_async_exn (main port) with
  | None -> Caml.Printf.printf "none\n"
  | Some s -> Caml.Printf.printf "some: %s\n" s
