open Core
open Graph
open Graphics

module Time = struct
         
  open Unix
              
  let utime f x =                                                   
    let u = (times()).tms_utime in                                  
    let y = f x in
    let ut = (times()).tms_utime -. u in
      y, ut

  let print s f x = 
    let (y,ut) = utime f x in
      Printf.printf "%s: user time: %2.2fs\n" s ut; y

end

module GG = GameGraph
module G = GG.G
module C = Oper.Choose(G)

module H = Hashtbl.Poly

let all_pairs_shortest_path (g : G.t) =
  let dists = H.create ~size:97 () in
  G.iter_vertex (fun v ->
      let h = GG.one_to_all_shortest_path g v in
      H.iteri h ~f:(fun ~key:v2 ~data:d ->
        H.set dists ~key:(v,v2) ~data:d
      )
    ) g;
  dists




let test_graph_algos () =
  Random.self_init ();
  Printf.printf "random: %d\n" (Random.int 10000);
  let g = G.Rand.graph ~v:20 ~e:36 () in
  (* G.dot_output g "test.dot"; *)
  (*let v = C.choose_vertex g in*)
  let span = Time.print "spanning tree" G.spanningtree g in
  let (n, scc) = Time.print "strongly connected components" G.Components.scc g in
  (*let all_s_paths = Time.print "all shortest paths" J1.all_pairs_shortest_paths g in*)
  (*let bf = Time.print "Bellman Ford" G.bellman_ford g v in*)
  Printf.printf "length of spanning tree list: %d\n" (List.length span);
  Printf.printf "number of strongly connected components: %d\n" n;
  ()

let is_member g n =
  Printf.printf "%d %smember\n" (G.V.label n) (if G.mem_vertex g n then "is a " else "is not a ")

let sucs g n =
  Printf.printf "%d has %d succesors\n" (G.V.label n) (List.length (G.succ g n))

let test_graph_basics () =
  let g = G.create () in
  let v1 = G.V.create 1 in
  let v2 = G.V.create 2 in
  let v3 = G.V.create 3 in
  let n1 = G.V.create 1 in
    G.add_vertex g v1;
    G.add_vertex g v2;
    G.add_vertex g v3;
    G.add_edge g v1 v2;
    G.add_edge g v2 v3;
    G.add_edge g v3 v1;
    is_member g n1;
  let n2 = G.find_vertex g 1 in
    is_member g n2;
    sucs g n2;
    let tmp = B64.encode (Marshal.to_string g []) in
    let g2 = Marshal.from_string (B64.decode tmp) 0 in
    sucs g2 n2;
  let n3 = G.find_vertex g2 2 in
  sucs g2 n3

module CG = ComponentGraph
let test_component_graph () =
  let map = GameGraphUtil.load_map_from_file "test_graphs/lambda.json" in
(*   GameGraphUtil.display_with ~app:"okular" map ; *)
  let cg = CG.init map in
  CG.display_with ~out_file:"dumps/graphtest1" cg ;
  (* There should be 4 components *)
  assert (CG.HSet.length cg.CG.components = 4);
  CG.display_with ~out_file:"dumps/graphtest2" cg ;
  CG.claim_edge cg (G.V.create 25) (G.V.create 18) ;
  assert (CG.HSet.length cg.CG.components = 5);
  CG.display_with ~out_file:"dumps/graphtest3a" cg ;
  CG.claim_edge cg (G.V.create 27) (G.V.create 25) ;
  CG.display_with ~out_file:"dumps/graphtest3b" cg ;
  CG.claim_edge cg (G.V.create 22) (G.V.create 18) ;
  CG.display_with ~out_file:"dumps/graphtest3c" cg ;
  (* There should now be 3 components *)
  assert (CG.HSet.length cg.CG.components = 3);
  let comp = Option.value_exn (CG.get_component cg (G.V.create 22)) in
  let () =
    let CG.C compvert = comp in
    assert (G.succ cg.CG.graph compvert |> List.length = 4);
  in
  CG.display_with ~out_file:"dumps/graphtest3" cg ;
  CG.claim_edge cg (G.V.create 37) (G.V.create 35) ;
  CG.claim_edge cg (G.V.create 35) (G.V.create 23) ;
  assert (CG.HSet.length cg.CG.components = 3);
  CG.display_with ~out_file:"dumps/graphtest4" cg ;
  let comp2 = Option.value_exn (CG.get_component cg (G.V.create 37)) in
  let () =
    let CG.C compvert2 = comp2 in
    assert (G.succ cg.CG.graph compvert2 |> List.length = 3) in
  CG.claim_edge cg (G.V.create 27) (G.V.create 23) ;
  assert (CG.HSet.length cg.CG.components = 2);
  let comp = Option.value_exn (CG.get_component cg (G.V.create 37)) in
  let () =
    let CG.C compvert = comp in
    assert (G.succ cg.CG.graph compvert |> List.length = 7) in
  CG.display_with ~out_file:"dumps/graphtest5" cg ;
  (* Path finding *)
  (* let () = *)
  (*   let path = CG.path_component_to_vertex cg comp (G.V.create 3) in *)
  (*   let edge = *)
  (*       match path with *)
  (*       | [edge] -> edge *)
  (*       | _  -> failwith "Path not of length 1" in *)
  (*   assert (G.E.src edge = G.V.create 25) ; *)
  (*   assert (G.E.dst edge = G.V.create 3) in *)
  (* let () = *)
  (*   CG.display_with ~out_file:"dumps/graphtest6" cg ; *)
  (*   match CG.get_component cg (G.V.create 32) with *)
  (*   | None -> failwith "No component with mine 32?" *)
  (*   | Some comp -> *)
  (*       let path = CG.path_component_to_vertex cg comp (G.V.create 3) in *)
  (*       (\* Note here that the path is really 5 long, but 1 edge is already claimed! *\) *)
  (*       assert (List.length path = 4) in *)
  (* let () = *)
  (*   let _ = CG.path_component_to_vertex cg comp (G.V.create 14) in *)
  (*   CG.opponent_claimed_edge cg (G.V.create 11) (G.V.create 14) ; *)
  (*   match CG.get_component cg (G.V.create 32) with *)
  (*   | None -> failwith "No component with mine 32?" *)
  (*   | Some comp -> *)
  (*      try *)
  (*       let _ = CG.path_component_to_vertex cg comp (G.V.create 14) in *)
  (*       assert false *)
  (*      with _ -> () in *)
  ()

let test_multi_source_bfs () =
  let g = G.create () in
  let sA = G.V.create 0 in
  let sB = G.V.create 1 in
  let vA1 = G.V.create 2 in
  let vA2 = G.V.create 3 in
  let vB1 = G.V.create 4 in
  let vB2 = G.V.create 5 in
  let vIgnored = G.V.create 6 in
  List.iter ~f:(G.add_vertex g) [sA; sB; vA1; vA2; vB1; vB2];
  List.iter ~f:(fun (v1, v2) -> G.add_edge g v1 v2) [
    sA, vA1;
    sA, vA2;
    vA1, vA2;
    vA1, vB1;
    vA1, sB;
    vA2, vB1;
    vB1, sB;
    sB, vB2;
    sB, vIgnored;
  ];
  let v_to_source =
    GameGraphUtil.multi_source_bfs g
      ~sources:[sA; sB]
      ~ignored_edges:[(vIgnored, sB)]
  in
  Hashtbl.Poly.iteri v_to_source (fun ~key:v ~data:source ->
    Printf.eprintf "%d belongs to %d\n" v source;
  )

let test_mine_neighbors () =
  let test_map name max_edges =
    let map = GameGraphUtil.load_map_from_file (Printf.sprintf "test_graphs/%s.json" name) in
    let dist_map = GG.distance_to_mine map in
    let mine_neighbors = MineNeighbors.mine_neighbors ~map:map ~dist_map:dist_map ~max_edges:max_edges in
    (* GameGraphUtil.display_with ~out_file:(Printf.sprintf "dumps/test_mine_neighbors_%s" name) map; *)
    GameGraphUtil.display_with ~out_file:(Printf.sprintf "dumps/test_mine_neighbors_%sN" name)
      { GG.graph=mine_neighbors; mines=map.GG.mines } 
  in
  test_map "lambda" 3;
  test_map "disconnected" 10;
  (* test_map "oxford-center-sparse" 25; *)
  test_map "oxford2-sparse-2" 50;
  (* test_map "van-city-sparse" 50; *)
  ()
  

let () =
  test_graph_algos () ;
  test_graph_basics () ;
  test_component_graph () ;
  test_multi_source_bfs () ;
  test_mine_neighbors () ;
  ()
