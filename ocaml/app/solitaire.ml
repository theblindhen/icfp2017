open Core

module GM = GameMessages
module GG = GameGraph
module G = GG.G
module H = Hashtbl.Poly
module HSet = Hash_set.Poly

type config = {
  log_pngs: bool;
  options: bool;
  map_file: string option;
  ai_names_rev: string list;
}

let parse_args =
  List.fold_left
    ~init:{
      log_pngs = false;
      options = false;
      map_file = None;
      ai_names_rev = [];
    }
    ~f:(fun config -> function
      | "--log-pngs" -> { config with log_pngs = true }
      | "--options" -> { config with options = true }
      | "--help" -> failwith "Help: read the source code"
      | s when String.is_prefix ~prefix:"-" s -> failwith "Invalid option"
      | arg ->
          if config.map_file = None
          then { config with map_file = Some arg }
          else { config with ai_names_rev = arg :: config.ai_names_rev }
    )

let () =
  let config =
    parse_args (
      Array.to_list (Array.slice Sys.argv 1 (Array.length Sys.argv))
    )
  in
  let map_file = Option.value_exn ~message:"No map given" config.map_file in
  let map = GameGraphUtil.load_map_from_file map_file in
  let players = PlayerBench.players in
  let ai_modules =
    config.ai_names_rev
    |> List.rev_map ~f:(List.Assoc.find_exn ~equal:String.equal players)
  in
 if List.is_empty ai_modules then failwith "No ai names given";
  Sys.command_exn "rm -rf solitaire.out";
  Sys.command_exn "mkdir solitaire.out";
  let turns = G.nb_edges map.GG.graph in
  let punters = List.length ai_modules in
  let ai_name punter = List.nth_exn config.ai_names_rev (punters - punter - 1) in
  let extensions = if config.options then [ GM.Options ] else [] in
  let initial_state = OfflineMode.init_istate 0 punters (GG.map_to_game_message map) extensions in
  let hstate = OfflineMode.init_hstate initial_state in
  let () =
    let sexts = "[ "^ (List.map extensions ~f:GM.extension_to_string |> String.concat ~sep:", ") ^" ]" in
    Log.log "Game setup:\n\tThere are %d punters\n\tThe map has %d sites and %d mines and %d rivers\n\tExtensions enabled are %s\n" punters (GG.G.nb_vertex map.GG.graph) (List.length map.GG.mines) (GG.G.nb_edges map.GG.graph) sexts
  in
  let ais =
    List.mapi ai_modules ~f:(fun punterid (ai : (module Player.AI)) ->
      PlayerBench.init_player ai 
        { initial_state with Player. punter = punterid })
  in
  let claims = H.create ~size:97 () in
  (*H.set claims ~key:punterid ~data:[];*)
  let select_ai punter = 
    match List.nth ais punter with
    | None -> failwith "No such AI dummy"
    | Some ai -> ai in
  let opponent_moved move turn =
    List.range ~stop:`exclusive 1 punters
    |> List.iter ~f:(fun off ->
         let punter = (turn + off) % punters in
         let { PlayerBench. opponent_moved } = select_ai punter in
         opponent_moved move)
  in
  let time punter f x = 
    let stime = Unix.gettimeofday () in
    let ret = f x in
    let etime = Unix.gettimeofday () in
    let time = etime -. stime in
    Log.log "punter %d (%s) used %fs to choose a move\n" punter (ai_name punter) time;
    ret
  in
  let total_options = if config.options then List.length map.GG.mines else 0 in
  let options_used = H.create ~size:punters () in
  (* set of all currently optioned and claimed edges stored as 
   * pairs (src, dst) with the invariant that src <= dst *)
  let optioned_edges = HSet.create ~size:(punters*10) () in
  let claimed_edges = HSet.create ~size:1000 () in
  List.iter (List.range ~stop:`exclusive 0 punters) ~f:(fun punter ->
        H.set options_used punter 0);
  let rec edges = function
  | [] -> []
  | [x] -> []
  | x::y::t -> (if x < y then (x, y) else (y, x))::edges (y::t)
  in
  (* TODO: implement all validation checks *)
  let validate_move move punter =
    assert(GM.move_by move = punter);
    match move with
    | GM.Option { source; target } ->
        assert(config.options);
        let edge = if source <= target then (source, target) else (target, source) in
        H.update options_used punter ~f:(function None -> 1 | Some n -> n + 1);
        assert(H.find_exn options_used punter <= (List.length map.GG.mines));
        assert(not (HSet.mem optioned_edges edge));
        assert(HSet.mem claimed_edges edge); 
        HSet.add optioned_edges edge
    | GM.Claim { source; target } -> 
        let edge = if source <= target then (source, target) else (target, source) in
        assert(not (HSet.mem claimed_edges edge));
        assert(GG.G.mem_edge map.GG.graph source target);
        HSet.add claimed_edges edge
    (* TODO: this assumes all splurges are claims rather than options *)
    | GM.Splurge { start; path } ->
        (* TODO: add flag to turn on splurges *)
        assert(List.length path > 0);
        List.iter (edges (start::path)) ~f:(fun edge ->
          assert(not (HSet.mem claimed_edges edge)); HSet.add claimed_edges edge)
    | _ -> ()
  in
  Log.log "Game setup: There are %d punters and the map has %d sites, %d mines and %d rivers\n" punters (GG.G.nb_vertex map.GG.graph) (List.length map.GG.mines) (GG.G.nb_edges map.GG.graph);
  List.range ~stop:`exclusive 0 turns
  |> List.iter 
      ~f:(fun turn ->
         Log.log "Turn %d\n" turn;
         let punter = turn % punters in
         let { PlayerBench. choose_move; we_moved } = select_ai punter in
         (* Hack hstate.rem_options *)
         hstate.Player.rem_options <- total_options - (H.find_exn options_used punter);
         let move = time punter choose_move (Player.set_punter hstate punter) in
         validate_move move punter;
         Player.update_hstate hstate move;
         we_moved move;
         opponent_moved move turn;
         let () =
            GM.iter_move move ~f:(fun ~punterid ~source ~target ->
                H.add_multi claims ~key:punterid ~data:(G.E.create source () target)
              )
         in
         let move_str = GM.string_of_move move in
         let out_file = Printf.sprintf "solitaire.out/turn%04d" turn in
         let message = Printf.sprintf "Turn %d: Punter %d %s" turn punter move_str in
         if config.log_pngs then
           GG.display_claims_map ~out_file map claims message;
      );
  if config.options then begin
    Log.log "%d options allowed\n" (List.length map.GG.mines); 
    H.iteri options_used ~f:(fun ~key:punter ~data:options ->
      Log.log "punter %d (%s) used %d options\n" punter (ai_name punter) options)
  end;
  Player.compute_scores hstate
  |> List.iter ~f:(fun (punter, score) ->
    Log.log "punter %d (%s) scored %d points\n" punter (ai_name punter) score)
