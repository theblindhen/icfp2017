open Solvuu_build.Std

let project_name = "myproject"
let version = "dev"

let lib = Project.lib "lib"
  ~bin_annot:()
  ~dir:"lib"
  ~style:`Basic
  ~install:(`Findlib "lib")
  ~thread:()
  ~findlib_deps:["core"; "yojson"; "ocamlgraph"; "base64"; "camlzip"]

let play = Project.app "play"
  ~bin_annot:()
  ~file:"app/play.ml"
  ~thread:()
  ~internal_deps:[lib]
  ~findlib_deps:["core"]

let graphtest = Project.app "graphtest"
  ~bin_annot:()
  ~file:"app/graphtest.ml"
  ~thread:()
  ~internal_deps:[lib]
  ~findlib_deps:["core"; "ocamlgraph"]

let solitaire = Project.app "solitaire"
  ~bin_annot:()
  ~file:"app/solitaire.ml"
  ~thread:()
  ~internal_deps:[lib]
  ~findlib_deps:["core"; "async"; "yojson"]

let wrapper = Project.app "wrapper"
  ~bin_annot:()
  ~file:"app/wrapper.ml"
  ~thread:()
  ~internal_deps:[]
  ~findlib_deps:["core"; "async"; "yojson"]

let () = Project.basic1 ~project_name ~version [
  lib;
  play;
  graphtest;
  wrapper;
  solitaire;
]
