"""
Inspired by: https://www.blog.pythonlibrary.org/2016/07/07/python-3-testing-an-intro-to-unittest/
which includes info about how to run certain tests selectively.
"""

import mylib
import unittest
 
class TestMylib(unittest.TestCase):

  def test_length(self):
    self.assertEqual(mylib.length([25, 42, 31]), 3)

if __name__ == '__main__':
    unittest.main()
