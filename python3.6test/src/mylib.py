import functools

def length(lst):
    r"""
    Compute the length of the given list

    TESTS::

        >>> from mylib import length
        >>> length([1,2,3])
        3
    """
    return functools.reduce(lambda acc, elm: acc + 1, lst, 0)

# def gaussian_elimination(matrix):
#     r"""
#     Return the reduced row echelon form of the input matrix.

#     TESTS::

#         >>> from numpy import array
#         >>> from mylib import gaussian_elimination
#         >>> a = array([[2.,4.,4.,4.], [1.,2.,3.,3.], [1.,2.,2.,2.], [1.,4.,3.,4.]])
#         >>> gaussian_elimination(a)
#     """
#     import scipy
#     return scipy.Matrix(matrix).rref()


if __name__ == "__main__":
    import doctest
    doctest.testmod()
